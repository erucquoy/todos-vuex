import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store.js'

import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'

import '@vuikit/theme'

Vue.use(Vuikit)
Vue.use(VuikitIcons)

Vue.config.productionTip = false

Vue.component('v-style', {
  render: function(createElement) {
    return createElement('style', this.$slots.default)
  }
})


/* router */
import beforeEach from './router/beforeEach'
router.beforeEach(beforeEach)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
