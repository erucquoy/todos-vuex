export default {
    /* Start names with $_    or   _   de preference */
    data() { return {

    }},
    computed: {

    },
    watch: {

    },
    methods: {

    },

    /* Lifecycle hooks */
    beforeCreate() {

    },
    created() {
        console.log('created from mixin')
    },
    beforeMount() {

    },
    mounted() {
        console.log('mounted from mixin')
    }
}