import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import beforeEach from './router/beforeEach';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/todos',
      name: 'todos',
      component: () => import(/* webpackChunkName: "todos" */ './views/Todos.vue'),
      meta: { requireAuth: true }
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import(/* webpackChunkName: "cart" */ './views/Cart.vue')
    },
    {
      path: '/products',
      name: 'products',
      component: () => import(/* webpackChunkName: "products" */ './views/Products.vue')
    },
    {
      path: '/products/:id',
      name: 'productDetails',
      component: () => import(/* webpackChunkName: "products" */ './views/ProductDetails.vue')
    },
    {
      path: '/forms',
      name: 'forms',
      component: () => import(/* webpackChunkName: "forms" */ './views/Forms.vue')
    },
    {
      path: '/tips',
      name: 'tips',
      component: () => import(/* webpackChunkName: "tips" */ './views/Tips.vue'),
      beforeEach: function () { console.log('before each tips'); next() }
    },
    {
      path: '/auth',
      component: () => import(/* webpackChunkName: "auth" */ './views/Auth.vue'),
      children: [
        {
          path: '',
          component: () => import(/* webpackChunkName: "auth" */ './views/Auth/Login.vue'),
          alias: 'login',
        },
        {
          path: 'signup',
          component: () => import(/* webpackChunkName: "auth" */ './views/Auth/Signup.vue')
        }
      ]
    },
  ]
})
