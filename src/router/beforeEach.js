import store from '@/store'

export default async (to, from, next) => {
    if (store.state.shop.products.length == 0) {
        await store.dispatch('shop/loadAllProducts')
        next()
    }
    else {
        next()
    }
}

