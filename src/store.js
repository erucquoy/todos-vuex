import Vue from 'vue'
import Vuex from 'vuex'

import shop from './store/shop.js'
import auth from './store/auth.js'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    shop: shop,
    auth,
  },
  state: {
    lastId: 1,
    todos: [ { id: 1, text: 'Hello Todo', done: false } ]
  },
  mutations: {

    setLastId (state, id) {
      state.lastId = id
    },
    pushTodo (state, todo) {
      state.todos.push(todo)
    },
    removeTodo (state, index) {
      state.todos.splice(index, 1)
    },
    doneTodo (state, index) {
      state.todos[index].done = !state.todos[index].done
    }

  },
  actions: {
    createTodo({ commit, state }, text) {
      const lastId = state.lastId + 1
      const todo = { id: lastId, text: text, done: false }
      commit('pushTodo', todo)
      commit('setLastId', lastId)
    },
    removeTodoFromId({ commit, getters }, id) {
      commit('removeTodo', getters.getIndexById(id))
    },
    doneTodoFromId({ commit, getters }, id) {
      commit('doneTodo', getters.getIndexById(id))
    },
  },
  getters: {
    getIndexById: state => id => {
      for (let i = 0; i < state.todos.length; i++) {
        if (state.todos[i].id == id) {
          return i
        }
      }
    },
  }
})
