import axios from 'axios'
import router from '@/router'

import {API} from '@/config'

export default {
    namespaced: true,
    state: {
        account: {
            id: null,
            username: '',
            password: '',
        }
    },
    mutations: {
        account(state, account) {
            state.account = account
        }
    },
    actions: {
        async login({ commit }, { username, password }) {
            try {
                const {data} = await axios.get(`${API}/v2/accounts?username=${username}&password=${password}`)
                if (data.length == 0) {
                    
                } else {
                    commit('account', data[0])
                }
            }
            catch (err) {
                console.error
            }
        },
        async signup({ commit }, { username, password }) {
            try {
                console.log(username, password)
                const {data} = await axios.post(`${API}/v2/accounts`, { username, password })
                console.log(data)
            }
            catch (err) {
                console.error
            }
        },
        async removeAllEmpty() {
            const {data} = await axios.get(`${API}/v2/accounts`)
           for (let i in data) {
               const acc = data[i]
               if (acc.username == "") {
                await axios.delete(`${API}/v2/accounts/${acc.id}`)
               }
           }
        }
    }
}