import axios from 'axios'
import { API } from '@/config'

export default {
    namespaced: true,
    state: {
        products: [],
        cart: [],
        per_page: 30,
    },
    mutations: {
        setProducts(state, products) {
            state.products = products
        },
        setPerPage(state, per_page) {
            state.per_page = per_page
        },
        addToCart(state, cp) {
            state.cart.push(cp)
        },
        incQuantity(state, i) {
            state.cart[i].quantity++
        },
        decQuantity(state, i) {
            state.cart[i].quantity--
        },
        removeCart(state, i) {
            state.cart.splice(i, 1)
        }
    },
    actions: {
        async loadAllProducts({ commit }) {
            try {
                const response = await axios.get(`${API}/products?per_page=1000`)
                commit('setProducts', response.data.products)
            }
            catch (err) {
                console.error(err)
            }
        },
        addToCart({ commit, getters, state }, id) {
            const index = getters.cartIndex(id)
            if (!state.cart[index]) {
                const cartProduct = {...getters.productById(id), quantity: 1}
                commit('addToCart', cartProduct)
            } else {
                commit('incQuantity', index) 
            }           
        },
        incQuantity({ getters, commit }, id) {
            const index = getters.cartIndex(id)
            commit('incQuantity', index)
        },
        decQuantity({ getters, commit, state }, id) {
            const index = getters.cartIndex(id)
            if (state.cart[index].quantity <= 1) {
                return commit('removeCart', index)
            }
            commit('decQuantity', index)
        },
        removeCart({ getters, commit }, id) {
            const index = getters.cartIndex(id)
            commit('removeCart', index)
        }
    },
    getters: {
        productById: state => id => {
            for (let i = 0; i < state.products.length; i++) {
              if (state.products[i].id == id) {
                return state.products[i]
              }
            }
        },
        productPage: state => page => {
            const startIndex = page * state.per_page
            const endIndex = startIndex + state.per_page
            return state.products.slice(startIndex, endIndex)
        },
        cartById: state => id => {
            for (let i = 0; i < state.cart.length; i++) {
              if (state.cart[i].id == id) {
                return state.cart[i]
              }
            }
        },
        cartIndex: state => id => {
            for (let i = 0; i < state.cart.length; i++) {
              if (state.cart[i].id == id) {
                  console.log('id', id, 'i', i)
                return i
              }
            }
        },
    }
} 